# Docker Image for aurto

A docker image based on the [archlinux](https://hub.docker.com/_/archlinux) image running [aurto](https://github.com/alexheretic/aurto) as a docker service.

## Usage

Since `aurto` works based on `systemd` timers, the container needs to be started with additional permissions.
To run the container, execute the following command:

```
docker run \
-d \
--name <container_name> \
--cap-add SYS_ADMIN \
-e TZ="<timezone>" \
-v /sys/fs/cgroup:/sys/fs/cgroup:ro \
-v aurto_db:/var/cache/pacman/aurto \
-v aurto_config:/etc/aurto \
bifbofii/aurto
```

When the container is running, you should probably first add `aurto` itself to the aurto database with the following command:

```
docker exec -it --user aurto <container_name> aurto add aurto
```

After that, the container can be updated with the following command:

```
docker exec -it --user aurto <container_name> sudo pacman -Syu
```

From now on the normal `aurto` command need to be prefixed with the following to be run inside the docker container:

```
docker exec -it --user aurto <container_name>
```

It might be useful to set an alias for this prefix for easier execution.
This can be done in the following style:

```
alias aurto="docker exec -it --user aurto <container_name> aurto"
```

## HTTP serving

To serve the generated database via HTTP, the aurto image can be paired with a nginx webserver as shown in the following `docker-compose` example:

```
version: "3"
services:
  aurto:
    image: bifbofii/aurto:latest
    restart: unless-stopped
    cap_add:
      - SYS_ADMIN
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
      - aurto_conf:/etc/aurto
      - aurto_db:/var/cache/pacman/aurto
    environment:
      - TZ=Europe/Berlin

  aurto-nginx:
    image: nginx:latest
    restart: unless-stopped
    volumes:
      - aurto_db:/usr/share/nginx/html
    ports:
      - 8080:80

volumes:
  aurto_db:

  aurto_conf:
```

This can be combined with a reverse proxy setup based on the `jwilder/nginx-proxy` image.
It might also be useful to add HTTP basic authentication to this setup.
